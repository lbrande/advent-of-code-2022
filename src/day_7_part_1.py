#!/bin/python
import utils


class Directory:
    def __init__(self, parent: 'Directory' = None):
        self.parent = parent
        self.size: int = 0
        self.subdirectories: list['Directory'] = []

    def create_subdirectory(self) -> 'Directory':
        subdirectory = Directory(self)
        self.subdirectories.append(subdirectory)
        return subdirectory

    def add_file(self, size: int):
        self.size += size

    def total_size(self) -> int:
        return self.size + sum(
            subdirectory.total_size()
            for subdirectory in self.subdirectories)

    def total_small_size(self) -> int:
        total_small_size = sum(
                subdirectory.total_small_size()
                for subdirectory in self.subdirectories)
        total_size = self.total_size()
        if total_size <= 100000:
            total_small_size += total_size
        return total_small_size


root = Directory()
current_directory = root

for line in utils.read_input(7).splitlines()[2:]:
    match line.split():
        case ['$', 'ls']:
            current_directory = current_directory.create_subdirectory()
        case [size, _] if not line.startswith('dir'):
            current_directory.add_file(int(size))
        case ['$', 'cd', '..']:
            current_directory = current_directory.parent

print(root.total_small_size())
