from os import path


def read_input(day: int) -> str:
    input_path = path.join(path.dirname(__file__), '../input/day_' + str(day))
    with open(input_path, 'r', encoding='utf-8') as file:
        return file.read()
