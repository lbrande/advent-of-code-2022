#!/bin/python
import utils


blocked: set[tuple[int, int]] = set()
max_y: int = 0

for line in utils.read_input(14).splitlines():
    points = [list(map(int, point.split(','))) for point in line.split(' -> ')]
    max_y = max(max_y, points[0][1])
    for i in range(1, len(points)):
        max_y = max(max_y, points[i][1])
        if points[i][0] == points[i - 1][0]:
            for y in range(
                min(points[i][1], points[i - 1][1]),
                max(points[i][1], points[i - 1][1]) + 1
            ):
                blocked.add((points[i][0], y))
        else:
            for x in range(
                min(points[i][0], points[i - 1][0]),
                max(points[i][0], points[i - 1][0]) + 1
            ):
                blocked.add((x, points[i][1]))

num_at_rest: int = 0
while True:
    x: int = 500
    y: int = 0
    while y < max_y:
        if (x, y + 1) not in blocked:
            y += 1
        elif (x - 1, y + 1) not in blocked:
            x -= 1
            y += 1
        elif (x + 1, y + 1) not in blocked:
            x += 1
            y += 1
        else:
            num_at_rest += 1
            blocked.add((x, y))
            break
    if y == max_y:
        break

print(num_at_rest)
