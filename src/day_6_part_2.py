#!/bin/python
import utils


def all_unique(characters: str) -> bool:
    for i, character_0 in enumerate(characters):
        for character_1 in characters[i + 1:]:
            if character_0 == character_1:
                return False
    return True


buffer = utils.read_input(6).splitlines()[0]

for i in range(14, len(buffer) - 1):
    if all_unique(buffer[i - 14:i]):
        print(i)
        break
