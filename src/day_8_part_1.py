#!/bin/python
import utils


trees = [
    list(map(int, list(line)))
    for line in utils.read_input(8).splitlines()]
visible = [[False] * len(trees[0]) for _ in range(len(trees))]

for row in range(len(trees)): # pylint: disable=consider-using-enumerate
    visible[row][0] = True
    max_tree = trees[row][0]
    for column in range(1, len(trees[row])):
        if trees[row][column] > max_tree:
            visible[row][column] = True
            max_tree = trees[row][column]
    visible[row][-1] = True
    max_tree = trees[row][-1]
    for column in range(len(trees[row]) - 2, -1, -1):
        if trees[row][column] > max_tree:
            visible[row][column] = True
            max_tree = trees[row][column]

for column in range(len(trees[0])):
    visible[0][column] = True
    max_tree = trees[0][column]
    for row in range(1, len(trees)):
        if trees[row][column] > max_tree:
            visible[row][column] = True
            max_tree = trees[row][column]
    visible[-1][column] = True
    max_tree = trees[-1][column]
    for row in range(len(trees) - 2, -1, -1):
        if trees[row][column] > max_tree:
            visible[row][column] = True
            max_tree = trees[row][column]

print(sum(map(sum, visible)))
