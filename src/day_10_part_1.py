#!/bin/python
import utils


cycle: int = 0
x: int = 1
signal_strengths = [0]

for instruction in utils.read_input(10).splitlines():
    cycle += 1
    signal_strengths.append(cycle * x)
    match instruction.split():
        case ['addx', v]:
            cycle += 1
            signal_strengths.append(cycle * x)
            x += int(v)

print(
    signal_strengths[20] + signal_strengths[60] + signal_strengths[100] +
    signal_strengths[140] + signal_strengths[180] + signal_strengths[220])
