#!/bin/python
import utils


elves = utils.read_input(1).split('\n\n')
elves_calories = [sum(map(int, foods.splitlines())) for foods in elves]

total_calories: int = 0
for _ in range(3):
    max_calories = max(elves_calories)
    total_calories += max_calories
    elves_calories.remove(max_calories)

print(total_calories)
