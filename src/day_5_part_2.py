#!/bin/python
import utils


input_parts = utils.read_input(5).split('\n\n')

crates = input_parts[0].splitlines()
stacks = [
    [
        height[index]
        for height in reversed(crates[:-1])
        if height[index] != ' ']
    for index in range(1, len(crates[-1]), 4)]

for step in input_parts[1].splitlines():
    steps_parts = step.split()
    temp_stack: list[str] = []
    for _ in range(int(steps_parts[1])):
        temp_stack.append(stacks[int(steps_parts[3]) - 1].pop())
    for _ in range(int(steps_parts[1])):
        stacks[int(steps_parts[5]) - 1].append(temp_stack.pop())

print("".join(stack.pop() for stack in stacks))
