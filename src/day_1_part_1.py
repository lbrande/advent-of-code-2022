#!/bin/python
import utils


elves = utils.read_input(1).split('\n\n')

print(max(sum(map(int, foods.splitlines())) for foods in elves))
