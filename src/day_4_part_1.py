#!/bin/python
import utils


def str_to_range(range_str: str) -> range:
    range_str_parts = range_str.split('-')
    return range(int(range_str_parts[0]), int(range_str_parts[1]) + 1)


def is_section_contained(section_0: str, section_1: str) -> bool:
    range_0 = str_to_range(section_0)
    range_1 = str_to_range(section_1)
    return (
        range_0.start <= range_1.start and range_0.stop >= range_1.stop or
        range_1.start <= range_0.start and range_1.stop >= range_0.stop)


pairs = [line.split(',') for line in utils.read_input(4).splitlines()]

print(sum(is_section_contained(pair[0], pair[1]) for pair in pairs))
