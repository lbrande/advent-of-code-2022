#!/bin/python
import functools
import utils


def parse_packet(packet: str, stack: list[list]):
    if not packet:
        return
    match packet[0]:
        case '[':
            stack.append([])
            parse_packet(packet[1:], stack)
        case ']':
            if len(stack) > 1:
                child = stack.pop()
                stack[-1].append(child)
            parse_packet(packet[1:], stack)
        case ',':
            parse_packet(packet[1:], stack)
        case _:
            end_of_number = [
                index
                for index, character in enumerate(packet)
                if character in '],'][0]
            stack[-1].append(int(packet[:end_of_number]))
            parse_packet(packet[end_of_number:], stack)


def compare(left: list, right: list) -> int:
    if not left or not right:
        return bool(left) - bool(right)
    if isinstance(left[0], int) and isinstance(right[0], int):
        if left[0] == right[0]:
            return compare(left[1:], right[1:])
        return left[0] - right[0]
    left_0 = [left[0]] if isinstance(left[0], int) else left[0]
    right_0 = [right[0]] if isinstance(right[0], int) else right[0]
    if compare(left_0, right_0) == 0:
        return compare(left[1:], right[1:])
    return compare(left_0, right_0)


packets = []

for packet in utils.read_input(13).splitlines():
    if packet:
        stack = [[]]
        parse_packet(packet, stack)
        packets.append(stack[0])

packets.append([[2]])
packets.append([[6]])

packets.sort(key=functools.cmp_to_key(compare))

print((packets.index([[2]]) + 1) * (packets.index([[6]]) + 1))
