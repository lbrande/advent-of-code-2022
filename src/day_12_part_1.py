#!/bin/python
from collections import deque
import utils


def height(height_map: list[list[str]], row: int, column: int) -> int:
    match height_map[row][column]:
        case 'S':
            return 0
        case 'E':
            return 25
        case _:
            return ord(height_map[row][column]) - ord('a')


height_map = [list(line) for line in utils.read_input(12).splitlines()]
visited = [[False] * len(height_map[0]) for _ in range(len(height_map))]

start = [
    (row, column)
    for row in range(len(height_map))
    for column in range(len(height_map[0]))
    if height_map[row][column] == 'S'][0]

queue: deque[tuple[tuple[int, int], int]] = deque([(start, 0)])
visited[start[0]][start[1]] = True

while queue:
    (row, column), steps = queue.popleft()
    if height_map[row][column] == 'E':
        print(steps)
        break
    for new_row, new_column in (
        (row + 1, column),
        (row - 1, column),
        (row, column + 1),
        (row, column - 1)
    ):
        if (
            0 <= new_row < len(height_map)
            and 0 <= new_column < len(height_map[new_row])
            and not visited[new_row][new_column]
            and height(height_map, new_row, new_column)
            <= height(height_map, row, column) + 1
        ):
            queue.append(((new_row, new_column), steps + 1))
            visited[new_row][new_column] = True
