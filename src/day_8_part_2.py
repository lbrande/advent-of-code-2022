#!/bin/python
import utils


trees = [
    list(map(int, list(line)))
    for line in utils.read_input(8).splitlines()]

max_score: int = 0
for row in range(len(trees)): # pylint: disable=consider-using-enumerate
    for column in range(len(trees[row])):
        score: int = 1
        blocked_row = row
        while blocked_row > 0:
            blocked_row -= 1
            if trees[blocked_row][column] >= trees[row][column]:
                break
        score *= row - blocked_row
        blocked_row = row
        while blocked_row < len(trees) - 1:
            blocked_row += 1
            if trees[blocked_row][column] >= trees[row][column]:
                break
        score *= blocked_row - row
        blocked_column = column
        while blocked_column > 0:
            blocked_column -= 1
            if trees[row][blocked_column] >= trees[row][column]:
                break
        score *= column - blocked_column
        blocked_column = column
        while blocked_column < len(trees[row]) - 1:
            blocked_column += 1
            if trees[row][blocked_column] >= trees[row][column]:
                break
        score *= blocked_column - column
        max_score = max(max_score, score)

print(max_score)
