#!/bin/python
import utils


def pull_tail(tail: tuple[int, int], head: tuple[int, int]) -> tuple[int, int]:
    head_x, head_y = head
    tail_x, tail_y = tail
    xdiff = head_x - tail_x
    ydiff = head_y - tail_y
    if abs(xdiff) > 1 and ydiff == 0:
        return (tail_x + xdiff // abs(xdiff), tail_y)
    elif abs(ydiff) > 1 and xdiff == 0:
        return (tail_x, tail_y + ydiff // abs(ydiff))
    elif (abs(xdiff) > 1 or abs(ydiff) > 1) and xdiff != 0 and ydiff != 0:
        return (
            tail_x + xdiff // abs(xdiff), tail_y + ydiff // abs(ydiff))
    return tail


visited: set[tuple[int, int]] = {(0, 0)}
tails: list[tuple[int, int]] = [(0, 0)] * 10

for [direction, steps] in list(
    map(str.split, utils.read_input(9).splitlines())
):
    for _ in range(int(steps)):
        head_x, head_y = tails[0]
        match direction:
            case 'L':
                tails[0] = (head_x - 1, head_y)
            case 'R':
                tails[0] = (head_x + 1, head_y)
            case 'U':
                tails[0] = (head_x, head_y + 1)
            case 'D':
                tails[0] = (head_x, head_y - 1)
        for i in range(1, len(tails)):
            tails[i] = pull_tail(tails[i], tails[i - 1])
        visited.add(tails[-1])

print(len(visited))
