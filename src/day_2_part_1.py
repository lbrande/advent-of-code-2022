#!/bin/python
import utils


rounds = utils.read_input(2).splitlines()
score_map = {
    'A X': 4,
    'A Y': 8,
    'A Z': 3,
    'B X': 1,
    'B Y': 5,
    'B Z': 9,
    'C X': 7,
    'C Y': 2,
    'C Z': 6,
}

print(sum(score_map[round] for round in rounds))
