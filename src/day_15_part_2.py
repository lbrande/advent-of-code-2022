#!/bin/python
import utils


sensors: list[tuple[tuple[int, int], int]] = []
lines: list[int] = []

for parts in (
    [int(part.split(',')[0].split(':')[0]) for part in line.split('=')[1:]]
    for line in utils.read_input(15).splitlines()
):
    sensors.append(
        (
            (parts[0], parts[1]),
            abs(parts[2] - parts[0]) + abs(parts[3] - parts[1])))

sensors.sort(key=lambda sensor: sensor[0][0])

for i, sensor_0 in enumerate(sensors[:-1]):
    pos_0, distance_0 = sensor_0
    for sensor_1 in sensors[i + 1:]:
        pos_1, distance_1 = sensor_1
        if (
            abs(pos_1[0] - pos_0[0]) + abs(pos_1[1] - pos_0[1]) -
            (distance_0 + distance_1) == 2
        ):
            lines.append(
                pos_0[0] + distance_0 + 1 +
                pos_0[1] * (pos_1[1] - pos_0[1]) // abs(pos_1[1] - pos_0[1]))

print((lines[0] + lines[1]) * 2000000 + abs(lines[0] - lines[1]) // 2)
