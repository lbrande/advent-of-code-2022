#!/bin/python
import utils


def update_crt(crt: list[list[bool]], cycle: int, x: int):
    if abs(cycle % 40 - x) < 2:
        crt[cycle // 40][cycle % 40] = True


cycle: int = 0
x: int = 1
crt = [[False] * 40 for _ in range(6)]

for instruction in utils.read_input(10).splitlines():
    update_crt(crt, cycle, x)
    cycle += 1
    match instruction.split():
        case ['addx', v]:
            update_crt(crt, cycle, x)
            cycle += 1
            x += int(v)

for row in crt:
    print(''.join(['#' if pixel else '.' for pixel in row]))
