#!/bin/python
import utils


def is_blocked(blocked: set[(int, int)], point: tuple[int, int]) -> bool:
    return (point[0], point[1]) in blocked or abs(500 - point[0]) > point[1]


blocked: set[tuple[int, int]] = set()
bottom_y: int = 0

for line in utils.read_input(14).splitlines():
    points = [list(map(int, point.split(','))) for point in line.split(' -> ')]
    bottom_y = max(bottom_y, points[0][1])
    for i in range(1, len(points)):
        bottom_y = max(bottom_y, points[i][1])
        if points[i][0] == points[i - 1][0]:
            for y in range(
                min(points[i][1], points[i - 1][1]),
                max(points[i][1], points[i - 1][1]) + 1
            ):
                blocked.add((points[i][0], y))
        else:
            for x in range(
                min(points[i][0], points[i - 1][0]),
                max(points[i][0], points[i - 1][0]) + 1
            ):
                blocked.add((x, points[i][1]))

bottom_y += 2

for y in range(1, bottom_y):
    for x in range(500 - y, 500 + y + 1):
        if (
            is_blocked(blocked, (x - 1, y - 1)) and
            is_blocked(blocked, (x, y - 1)) and
            is_blocked(blocked, (x + 1, y - 1))
        ):
            blocked.add((x, y))

print(bottom_y * bottom_y - len(blocked))
