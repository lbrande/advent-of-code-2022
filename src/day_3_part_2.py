#!/bin/python
import utils


def priority(item: str) -> int:
    if ord(item) < ord('a'):
        return ord(item) - ord('A') + 27
    return ord(item) - ord('a') + 1


def find_common(rucksacks: list[str]) -> str:
    return [
        item
        for item in rucksacks[0]
        if all(item in rucksack for rucksack in rucksacks[1:])][0]


rucksacks = utils.read_input(3).splitlines()

total_priority: int = 0
for i in range(0, len(rucksacks), 3):
    total_priority += priority(find_common(rucksacks[i:i+3]))

print(total_priority)
