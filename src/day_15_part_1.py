#!/bin/python
import utils


ranges: list[range] = []

for parts in (
    [int(part.split(',')[0].split(':')[0]) for part in line.split('=')[1:]]
    for line in utils.read_input(15).splitlines()
):
    distance = abs(parts[0] - parts[2]) + abs(parts[1] - parts[3])
    overlap = distance - abs(parts[1] - 2000000)
    if overlap > 0 or overlap == 0 and parts[3] != 2000000:
        if parts[3] == 2000000 and parts[2] < parts[0]:
            new_range = range(parts[0] - overlap + 1, parts[0] + overlap + 1)
        elif parts[3] == 2000000 and parts[2] > parts[0]:
            new_range = range(parts[0] - overlap, parts[0] + overlap)
        else:
            new_range = range(parts[0] - overlap, parts[0] + overlap + 1)
        ranges.append(new_range)
        for i in range(len(ranges) - 2, -1, -1):
            if new_range.start > ranges[i].stop:
                break
            if new_range.stop < ranges[i].start:
                ranges[i], ranges[i + 1] = ranges[i + 1], ranges[i]
            else:
                ranges[i] = range(
                    min(new_range.start, ranges[i].start),
                    max(new_range.stop, ranges[i].stop)
                )
                del ranges[i + 1]
                new_range = ranges[i]

print(sum(range_.stop - range_.start for range_ in ranges))
