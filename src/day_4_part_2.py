#!/bin/python
import utils


def str_to_range(range_str: str) -> range:
    range_str_parts = range_str.split('-')
    return range(int(range_str_parts[0]), int(range_str_parts[1]) + 1)


def does_section_overlap(section_0: str, section_1: str) -> bool:
    range_0 = str_to_range(section_0)
    range_1 = str_to_range(section_1)
    return range_0.start in range_1 or range_1.start in range_0


pairs = [line.split(',') for line in utils.read_input(4).splitlines()]

print(sum(does_section_overlap(pair[0], pair[1]) for pair in pairs))
