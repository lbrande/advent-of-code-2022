#!/bin/python
from typing import Callable
import utils


class Monkey:
    def __init__(
        self,
        definition: str
    ):
        lines = definition.splitlines()
        self.items: list[int] = list(
            map(int, lines[1].split(': ')[1].split(', ')))
        self.operation: Callable[[int], int] = eval( # pylint: disable=eval-used
            "lambda old: " + lines[2].split('= ')[1])
        self.check_divisible_by = int(lines[3].split(' ')[-1])
        self.throw_to_dict: dict[bool, int] = {
            True: int(lines[4].split(' ')[-1]),
            False: int(lines[5].split(' ')[-1])}

    def take_turn(self, monkeys: list['Monkey']) -> int:
        num_inspected = len(self.items)
        for item in self.items:
            item = self.operation(item) // 3
            self.throw(item, monkeys)
        self.items = []
        return num_inspected

    def throw(self, item: int, monkeys: list['Monkey']):
        monkeys[
            self.throw_to_dict[item % self.check_divisible_by == 0]
            ].items.append(item)

monkeys: list[Monkey] = []

for definition in utils.read_input(11).split('\n\n'):
    monkeys.append(Monkey(definition))

num_inspected = [0] * len(monkeys)

for _ in range(20):
    for i, monkey in enumerate(monkeys):
        num_inspected[i] += monkey.take_turn(monkeys)

monkey_business = max(num_inspected)
num_inspected.remove(monkey_business)
monkey_business *= max(num_inspected)

print(monkey_business)
