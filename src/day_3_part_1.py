#!/bin/python
import utils


def priority(item: str) -> int:
    if ord(item) < ord('a'):
        return ord(item) - ord('A') + 27
    return ord(item) - ord('a') + 1


def find_duplicate(rucksack: str) -> str:
    compartment_0 = rucksack[:len(rucksack) // 2]
    compartment_1 = rucksack[len(rucksack) // 2:]
    return [item for item in compartment_0 if item in compartment_1][0]


rucksacks = utils.read_input(3).splitlines()

print(sum(priority(find_duplicate(rucksack)) for rucksack in rucksacks))
